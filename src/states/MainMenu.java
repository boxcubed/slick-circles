package states;

import java.awt.Font;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import Objects.gameCircle;
import main.States;
import util.MenuButton;
import util.MenuListener;

public class MainMenu extends BasicGameState{
    private final int NOCHOICES = 3;
	 /* private int playersChoice = 0;
	
	    private final int START = 0;
	    private final int OPTIONS = 1;
	    private final int QUIT = 2;*/
//	    private String[] playersOptions=new String[NOCHOICES];
	   private MenuButton[] playersOptions;
	    private boolean exit = false;
	    private Font /*font,*/font1,f,cf;
	    private TrueTypeFont /*playersOptionsTTF,*/TitleTTF;
	    //private Color notChosen = new Color(153, 204, 255);
	    private String[] options = new String[4];
	    boolean displayoptions=false;
	    Image[] images=new Image[4];
	    long elapsedtime;
	    public ArrayList<gameCircle> list;
	   // MenuButton m;
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		// font = new Font ("Verdana", Font.PLAIN, 40);
		 font1 = new Font("Arial", Font.BOLD,50);
		 f=new Font("Verdana", Font.PLAIN, 40);
		 cf=new Font("Verdana", Font.BOLD, 40);
	       // playersOptionsTTF = new TrueTypeFont(font, true);
	        TitleTTF = new TrueTypeFont(font1, true);
	        playersOptions=new MenuButton[NOCHOICES];
	        initButtons(game,container);
	        //playersOptions[0] = "Start";
	        
	        options[0]="Options:";
	        options[1]="- 4k gameplay boiiii";
	        options[2]="- 320p (for rahul's pleb gpu)";
	        
	        images[0] = new Image("res/petithead.png");
	        images[1] = new Image("res/meme1.jpg");
	        images[2] = new Image("res/meme2.jpg");
	        images[3] = new Image("res/meme3.jpg");
	        list=new ArrayList<gameCircle>();
/* m=new MenuButton("Dont touch me!", new Font("Verdana", Font.ITALIC, 40), 0, 100, new MenuListener() {
				
				@Override
				public void rightclicked(MenuButton m) {
					// TODO Auto-generated method stub
					game.enterState(2);
				}
				
				@Override
				public void clicked(MenuButton m) {
					// TODO Auto-generated method stub
					game.enterState(1);
				}
				@Override
				public void chosen(MenuButton m){m.setMessage("Why u touch my Dick?! U  gay?");}
				@Override 
				public void notChosen(MenuButton m){m.setMessage("Don't touch my Dick!");}
			});m.setChosenColor(Color.red);*/
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		for(gameCircle c:list){
			c.render(g);
			
		}
	    TitleTTF.drawString(350, 50, "Circles");
	   
	    
		g.setColor(Color.green);
		g.fill(new Circle(500, 600, 80));
		  renderPlayersOptions(g);
	        if (exit) {
	            container.exit();
	        }
	       
	        
	        if(displayoptions){
	        	for(int i=0;i<3;i++){
	        		 g.drawString(options[i],500, i * 30 + 200);
	        	}
	        	
	        }
	        if(States.petitMode){
	        for(int i=1;i<4;i++){
				images[i].draw(800, (i * 10)*24-250, 300,250);
		    }images[0].draw(350,200,550,600);}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
	  
	for(MenuButton m:playersOptions){
		m.update(container);
	}
   	 Input input = container.getInput();
     /*if (input.isKeyPressed(Input.KEY_DOWN)) {
         if (playersChoice == (NOCHOICES - 1)) {
             playersChoice = 0;
         } else {
             playersChoice++;
         }
     }
     if (input.isKeyPressed(Input.KEY_UP)) {
         if (playersChoice == 0) {
             playersChoice = NOCHOICES - 1;
         } else {
             playersChoice--;
         }
     }
     if (input.isKeyPressed(Input.KEY_ENTER)) {
         switch (playersChoice) {
             case QUIT:
                 exit = true;
                 break;
             case START:
            	 game.enterState(1);
            	 break;
             case OPTIONS:
            	 displayoptions=true;
            	 break;
         }
     }*/
     if (input.isKeyPressed(Input.KEY_LEFT)) {
    	 displayoptions=false;
     }
     //rendering circle effect
     for(int i=0;i<list.size();i++){
			gameCircle c=list.get(i);
			c.update(delta,list);
			if(!c.exist){
				
				list.remove(i);
			}
			
	}
     //removing circles for memory
     if(elapsedtime >= 100) { list.add(new gameCircle(-100, -100, 20));
		elapsedtime=0;
		 } else elapsedtime += delta;  
	
	
	}

	private void renderPlayersOptions(Graphics g) {
        for (int i = 0; i < NOCHOICES; i++) {
           // if (playersChoice == i) {
               // playersOptionsTTF.drawString(100, i * 45 + 200, playersOptions[i]);
            	if(playersOptions[i].isChosen()) 
            		playersOptions[i].setY(i*55+200);
            	else
            		playersOptions[i].setY(i*55+200);
                playersOptions[i].setX(100);
               
                playersOptions[i].render(g);
            /*} else {
            	playersOptions[i].setChosen(false);
               //playersOptionsTTF.drawString(100, i * 45 + 200, playersOptions[i], notChosen);
            	 playersOptions[i].setX(100);
                 playersOptions[i].setY(i*45+200+10);
                 playersOptions[i].render(g);
           }*/
        }
    }
	private void initButtons(StateBasedGame game, GameContainer container){
		playersOptions[0]=new MenuButton("Start", f,cf ,1 ,1 , new MenuListener() {
			
			@Override
			public void rightclicked(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void notChosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void clicked(MenuButton m) {
				// TODO Auto-generated method stub
				game.enterState(1);
			}
			
			@Override
			public void chosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
		});
        //playersOptions[1] = "Settings";
playersOptions[1]=new MenuButton("Settings", f,cf ,1 ,1 , new MenuListener() {
			
			@Override
			public void rightclicked(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void notChosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void clicked(MenuButton m) {
				// TODO Auto-generated method stub
				if(displayoptions)displayoptions=false;else displayoptions=true;
			}
			
			@Override
			public void chosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
		});
       // playersOptions[2] = "Quit";
playersOptions[2]=new MenuButton("Quit", f,cf ,1 ,1 , new MenuListener() {
	
	@Override
	public void rightclicked(MenuButton m) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void notChosen(MenuButton m) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void clicked(MenuButton m) {
		// TODO Auto-generated method stub
		container.exit();
	}
	
	@Override
	public void chosen(MenuButton m) {
		// TODO Auto-generated method stub
		
	}
});
	}
	@Override
	public int getID() {
		return 0;
	}
	@Override
	public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
		gc.setMouseGrabbed(false);
	}

}
