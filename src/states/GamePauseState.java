package states;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.HorizontalSplitTransition;
import org.newdawn.slick.state.transition.Transition;

import javafx.scene.control.MenuBuilder;
import main.States;
import util.MenuButton;
import util.MenuListener;

public class GamePauseState extends BasicGameState{
	 
	    private MenuButton[] playersOptions = new MenuButton[3];
	    private boolean exit = false;
	    private Font font,f,cf;
	    private TrueTypeFont playersOptionsTTF, foo;
	    private Color notChosen = new Color(203, 204, 255);
	@Override
	public void init(GameContainer gc, StateBasedGame state) throws SlickException {
		 font = new Font("Verdana", Font.BOLD, 40);
		 f=new Font("Verdana", Font.PLAIN, 40);
		 cf=new Font("Verdana", Font.BOLD, 40);
	        playersOptionsTTF = new TrueTypeFont(font, true);
	        font = new Font ("Verdana", Font.PLAIN, 20);
	        foo = new TrueTypeFont(font, true);
	        initButton(state, gc);
	        
	}
	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for(MenuButton m:playersOptions)m.update(gc);

		if(gc.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			state.enterState(1);
		}
		if(gc.getInput().isKeyDown(Input.KEY_EQUALS)){
			gc.setClearEachFrame(false);
			
		}
		if(gc.getInput().isKeyDown(Input.KEY_DELETE)){
			
			gc.exit();
			
			
		}if(gc.getInput().isKeyPressed(Input.KEY_I)){
			
			if(States.petitMode){
				States.petitMode=false;
			}else States.petitMode=true;
			
			
		}
		  Input input = gc.getInput();
	      
	        if(input.isKeyDown(Input.KEY_M)){
				state.init(gc);
				state.enterState(0);
				
				
			}if(input.isKeyPressed(Input.KEY_D)){
				if(States.debugMode)
				States.debugMode=false;else States.debugMode=true;
				
				
			}
	        
		
		
	}
	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		g.drawString("Main Menu", 20, 20);
		   renderPlayersOptions(g);
	        if (exit) {
	            gc.exit();
	        }
	}
	
	  private void renderPlayersOptions(Graphics g) {
	        for (int i = 0; i < playersOptions.length; i++) {
	          MenuButton m=playersOptions[i];
	  
	         
	          m.render(g);
	              
	            
	        }
	    }
	
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 2;
	}
	void initButton(StateBasedGame state, GameContainer gc){
		
		playersOptions[0] = new MenuButton("Play", f, cf, 1, 1, new MenuListener() {
			
			@Override
			public void rightclicked(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void notChosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void clicked(MenuButton m) {
				// TODO Auto-generated method stub
				state.enterState(1);
			}
			
			@Override
			public void chosen(MenuButton m) {
				// TODO Auto-generated method stub
				
				
			}
		});
        playersOptions[1] = new MenuButton("Menu", f, cf, 1, 1, new MenuListener() {
			
			@Override
			public void rightclicked(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void notChosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void clicked(MenuButton m) {
				// TODO Auto-generated method 
				try {
					state.init(gc);
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				state.enterState(0, new FadeOutTransition(notChosen, 10), new HorizontalSplitTransition(Color.white));
				}
			
			@Override
			public void chosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
		});
        
 playersOptions[2] = new MenuButton("Settings", f, cf, 1, 1, new MenuListener() {
			
			@Override
			public void rightclicked(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void notChosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void clicked(MenuButton m) {
				// TODO Auto-generated method 
				
				}
			
			@Override
			public void chosen(MenuButton m) {
				// TODO Auto-generated method stub
				
			}
		});
 for(int i=0;i<playersOptions.length;i++){
	 MenuButton m=playersOptions[i];
	 m.orderButton(i, 40, 200);
	 m.getRect().setBounds(m.getX(),m.getY()+13,400,30);
	 m.setCollisionLock(true);}
	}
	@Override
	public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
		gc.setMouseGrabbed(false);
		
	}
}


