package Objects;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;

import main.States;

@SuppressWarnings("serial")
public class gameCircle extends Circle {
Random r;
Color c;
Color prevc;
Image image;
float x;
int startposx,tickcount=0;
float wiggle=0.09f,wavel;
float y=0-getRadius();
public boolean exist=true;
//float delta;
float speed=5;
boolean formula =true;
boolean corrupted=false;
boolean cos=false;



	public gameCircle(float centerPointX, float centerPointY, float radius) {
		super(centerPointX, centerPointY, radius);
		r=new Random();
		try {
			image = new Image("res/petithead.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
		c=color(r.nextInt(5));
		startposx=r.nextInt((int) ((States.WIDTH-radius)+10));
		wavel = r.nextInt(70)+50;
		setRadius((20f/480f)*States.HEIGHT);
		int i=r.nextInt(20);
		if(i<=9) formula=true; else formula=false;
	    if(i==20)corrupted=true; else corrupted=false;
		if(i<=19&&!(i<=9)){cos=true;}else{cos=false;}
		x=-100;
		y=-100;
		// TODO Auto-generated constructor stub
		
	}
	
	public void update(int delta,ArrayList<gameCircle> balls){
		//this.delta=delta;
		tickcount++;
		updateAlgorithm(delta);
		
	}
	
	public void render(Graphics g){
		if(States.petitMode){
		prevc=g.getColor();
		setCenterY(y);
		
		setCenterX(x);
		g.setColor(c); 
		image.draw(x-28,y-15,(120f/480f)*States.HEIGHT,(155f/480)*States.HEIGHT);
		if(States.debugMode==true) g.draw(this);
		g.setColor(prevc);
		}else{
			prevc=g.getColor();
			setCenterY(y);
			setCenterX(x);
			g.setColor(c); 
			g.fill(this);
			g.setColor(prevc);
		}
		
		
		
	}
	
	
		Color color(int color){
			switch(color){
			case 0: return Color.green;
			case 1: return Color.yellow;
			case 2:return Color.blue;
			case 3: return Color.red;
			case 4: return Color.pink;
			
					
			
			}
			return null;}

		void updateAlgorithm(int delta){
			if(formula)
				x=(float) (wavel*Math.sin(wiggle*tickcount)+startposx);else if(corrupted) x=(float) (wavel*Math.tan(wiggle*tickcount)+startposx);
				else if(cos){wavel=80;x=(float)(wavel*Math.cos(wiggle*tickcount)+startposx);}
				else x=(float) (wavel*Math.cos(wiggle*tickcount)+startposx);//change one to tan for cool effect
				if(!((getCenterY()-radius)>=States.HEIGHT)){
					y=(float) (getCenterY()+(delta/speed));}else exist=false;
				
		}

	

}
